/*
 * wakeup_monitor.c - PM wakeup minitor functionality
 *
 * Copyright (c) 2014 Lab126
 *
 */

#include <linux/kobject.h>
#include <linux/uaccess.h>
#include <linux/sysfs.h>
#include <linux/proc_fs.h>
#include <linux/sched.h>
#include <linux/export.h>
#include <linux/rtc.h>
#include <linux/workqueue.h>
#include <linux/syscore_ops.h>
#include <linux/kernel.h>
#include <linux/slab.h>
#include <linux/module.h>
#include <linux/jiffies.h>
#include <linux/spinlock.h>
#include <linux/notifier.h>
#include <linux/printk.h>
#include <linux/suspend.h>
#include <linux/interrupt.h>
#include <linux/irq.h>
#if defined(CONFIG_AMAZON_METRICS_LOG)
#include <linux/metricslog.h>
#include <linux/rtc.h>
#endif
#include "linux/wm.h"

#define BUFFER_LEN (8)
#define PWR_BUTTON_IRQ 291
#define WCNSS_TX 177
#define WCNSS_RX 178
#define SMD_MODEM 57
#define RTC_ALARM 326
#define RPM_IRQ 200
#define DEFAULT_CONTROL "1\0"	/* enabled by default */
#define WM_CONTROL_SIZE (1)
#define MAX_PROCESS_NAME (64)

#define DEFINE_OPEN_FUNC(name) proc_##name##_open

#define DEFINE_FILE_OPERATIONS(name)	\
	const struct file_operations wakeup_proc_##name##_fops = {	\
		.open = DEFINE_OPEN_FUNC(name),	\
		.read = seq_read,	\
		.llseek = seq_lseek,	\
		.release = single_release,	\
}

#define DEFINE_PROC_OPEN(name)	\
	int proc_##name##_open(struct inode *inode, struct file *filp)\
	{ \
		return single_open(filp, show_##name, NULL);	\
	}

enum irq_num {
	IRQ_UNKNOWN = 0,
	IRQ_WCNSS_RX,
	IRQ_MODEM,
	IRQ_RTC_ALARM,
	IRQ_TYPES,
};

struct wakeup_event_monitor {
	enum irq_num event;
	unsigned long count;
	ktime_t last_time;
	ktime_t total_time;
};

struct wakeup_process {
	char name[MAX_PROCESS_NAME];
	pid_t pid;
	pid_t uid;
	unsigned long count;
	struct list_head list;
};

static struct proc_dir_entry *proc_wakeup_monitor_dir;
static struct work_struct wakeup_defer_work;
static char control_buff[BUFFER_LEN];
static bool enabled = 1;	/* default is enabled */
static LIST_HEAD(wifi_wakeup_processes_list);
static LIST_HEAD(alarm_wakeup_processes_list);
static LIST_HEAD(modem_wakeup_processes_list);
static struct wakeup_event_monitor wakeup_counts[IRQ_TYPES];
static struct wakeup_event_monitor *last_wakeup;
static enum irq_num wakeup_count_update_item = IRQ_UNKNOWN;
static u32 last_vmin_count;
static struct wakeup_source wakeup_monitor_ws;


static int wakeup_process_alloc_and_add(struct list_head *head,
					const char *name,
					const pid_t pid, const pid_t uid)
{
	struct wakeup_process *wk =
		kzalloc(sizeof(struct wakeup_process), GFP_KERNEL);

	if (!wk) {
		pr_err
			("%s: Failed to alloc for wakeup_process\n",
			__func__);
		return -ENOMEM;
	}

	strlcpy(wk->name, name, MAX_PROCESS_NAME - 1);
	wk->pid = pid;
	wk->uid = uid;
	wk->count = 1;
	INIT_LIST_HEAD(&wk->list);
	list_add(&wk->list, head);

	return 0;
}

static int wakeup_process_find_and_increment(struct list_head *head,
					     const int uid)
{
	struct wakeup_process *wk, *t;
	list_for_each_entry_safe(wk, t, head, list) {
		if (wk->uid == uid) {
			pr_debug
				("%s: found the process=%s with"\
				"uid=%d, count=%lu\n",
				__func__, wk->name, uid, wk->count);
			wk->count++;
			return 1;
		}
	}
	return 0;
}

static ssize_t control_write(struct file *file, const char __user *buffer,
				size_t len, loff_t *pos)
{
	/* len -1 to assure the null-terminator character */
	ssize_t cp_len = len > (BUFFER_LEN - 1) ? (BUFFER_LEN - 1) : len;

	memset(control_buff, 0, BUFFER_LEN);
	if (copy_from_user(control_buff, buffer, cp_len))
		return -EFAULT;

	if (strncmp(control_buff, "1", WM_CONTROL_SIZE) == 0)
		enabled = 1;
	else
		enabled = 0;

	return cp_len;
}

static int show_control(struct seq_file *m, void *v)
{
	int len = strlen(control_buff);
	if (len <= 0) {
		pr_err("%s: ERROR control_buff string is invalid\n", __func__);
		return -EBUSY;
	}

	seq_printf(m, "%d\n", enabled);
	return 0;
}

/*
 * wifi/alarm/modem sysfs format:
 * wakeup_counts uevent_trigger_counts
 * locking is not necessary since the scan_wakeup_irq is invoked
 * with local irq disabled.
 * do we need spin lock? do we care race condition?
 */
static int show_wifi(struct seq_file *m, void *v)
{
	seq_printf(m, "%lu\t%lld\n", wakeup_counts[IRQ_WCNSS_RX].count,
		ktime_to_ms(wakeup_counts[IRQ_WCNSS_RX].total_time));
	return 0;
}

static int show_alarm(struct seq_file *m, void *v)
{
	seq_printf(m, "%lu\t%lld\n", wakeup_counts[IRQ_RTC_ALARM].count,
		ktime_to_ms(wakeup_counts[IRQ_RTC_ALARM].total_time));
	return 0;
}

static int show_modem(struct seq_file *m, void *v)
{
	seq_printf(m, "%lu\t%lld\n", wakeup_counts[IRQ_MODEM].count,
		ktime_to_ms(wakeup_counts[IRQ_MODEM].total_time));
	return 0;
}

static DEFINE_PROC_OPEN(control)
static DEFINE_PROC_OPEN(wifi)
static DEFINE_PROC_OPEN(alarm)
static DEFINE_PROC_OPEN(modem)

static const DEFINE_FILE_OPERATIONS(modem);
static const DEFINE_FILE_OPERATIONS(wifi);
static const DEFINE_FILE_OPERATIONS(alarm);
static const struct file_operations wakeup_proc_control_fops = {
	.open = proc_control_open,
	.read = seq_read,
	.write = control_write,
	.llseek = seq_lseek,
	.release = single_release,
};

extern int msm_rpmstats_read(u32 *);

static int is_wakeup_from_vmin(void)
{
	u32 vmin_count = 0;	/* tracking the last vmin counter */
	pr_debug("%s: Enter(cpu=%u, pid=%d)\n", __func__, smp_processor_id(),
		 current->pid);
	/* every wakeup read the count */
	if (msm_rpmstats_read(&vmin_count) == 0) {
		if (vmin_count > last_vmin_count) {
			pr_info("%s: wakeup from vmin\n", __func__);
			last_vmin_count = vmin_count;
			return 1;
		}
	}
	return 0;
}

/*
 * Notes:
 * we possibly can't send uevent to user space in the syscore_ops function
 * since the userspace already frozen. so we do it in the defered function

 * it is possible that we are in the middle of the function, the autosleep
 * thread start again. maybe take a wake lock and release it.

 * There is possible race that when we update the trigger count, the
 * userspace process is reading the alarm count, but it is ok, since the
 * userspace is just reading the count, it does not really matter the count
 * is getting updated while in the middle of reading.

 * another race condition is that the system enter suspend again in the middle
 * __pm_stay_awake() function, which is ok, we allow the system enter suspend \
 * before we update the trigger counts and sending the uevent, we just missed
 * one count, not bit deal. Since it is likely that the system enter suspend
 * event before the wakeup_defer_func() get chance (scheduled) to run.
 */
static void wakeup_defer_func(struct work_struct *work)
{
#ifdef CONFIG_AMAZON_METRICS_LOG
	char metrics_buffer[128];
#endif
	int woken_from_vmin = 0;
	/* hold a wake lock just to make sure we send the uevent */
	__pm_stay_awake(&wakeup_monitor_ws);
	if (wakeup_count_update_item != IRQ_UNKNOWN) {
		if (wakeup_count_update_item == IRQ_RTC_ALARM) {
			pr_debug
				("%s: cur_pid=%d,tgid=%d,g_pid=%d,"\
				"g_tgid=%d,cur=%s,g_cur=%s, uid=%d\n",
				__func__, current->pid, current->tgid,
				current->group_leader->pid,
				current->group_leader->tgid, current->comm,
				current->group_leader->comm, current_uid());
			if (!wakeup_process_find_and_increment
				(&alarm_wakeup_processes_list, current_uid())) {
				/* did not find the process,
				alloc a new one and add it */
				wakeup_process_alloc_and_add
					(&alarm_wakeup_processes_list,
					current->group_leader->comm,
					current->pid,
					current_uid());
			}
		}
		woken_from_vmin = is_wakeup_from_vmin();
	}
	__pm_relax(&wakeup_monitor_ws);

#ifdef CONFIG_AMAZON_METRICS_LOG
	snprintf(metrics_buffer, sizeof(metrics_buffer),
		 "power:wakeup_mon:wk_type=%d;cnt=%ld;DV;1,wk_vmin=%d;CT;1:NR",
		 wakeup_count_update_item,
		 wakeup_counts[wakeup_count_update_item].count,
		 woken_from_vmin);
	log_to_metrics(ANDROID_LOG_INFO, "kernel", metrics_buffer);
#endif
}

void scan_wakeup_irq(unsigned int irq_num)
{
	if (enabled) {
		switch (irq_num) {
		case WCNSS_RX:
			pr_debug("%s: wifi interrupt triggered\n", __func__);
			wakeup_count_update_item = IRQ_WCNSS_RX;
			wakeup_counts[wakeup_count_update_item].count++;
			wakeup_counts[wakeup_count_update_item].last_time =
				ns_to_ktime(sched_clock());
			wakeup_counts[wakeup_count_update_item].event =
				IRQ_WCNSS_RX;
			last_wakeup = &wakeup_counts[wakeup_count_update_item];
			schedule_work(&wakeup_defer_work);
			break;
		case RTC_ALARM:
			pr_debug("%s: alarm interrupt triggered\n", __func__);
			wakeup_count_update_item = IRQ_RTC_ALARM;
			/* clear the flag just incase
			 * alarm irq followed by wifi */
			wakeup_counts[wakeup_count_update_item].count++;
			wakeup_counts[wakeup_count_update_item].last_time =
				ns_to_ktime(sched_clock());
			wakeup_counts[wakeup_count_update_item].event =
			    IRQ_RTC_ALARM;
			last_wakeup = &wakeup_counts[wakeup_count_update_item];
			schedule_work(&wakeup_defer_work);
			break;
		case SMD_MODEM:
			pr_debug("%s: modem interrupt triggered\n", __func__);
			wakeup_count_update_item = IRQ_MODEM;
			wakeup_counts[wakeup_count_update_item].count++;
			wakeup_counts[wakeup_count_update_item].last_time =
				ns_to_ktime(sched_clock());
			wakeup_counts[wakeup_count_update_item].event =
			    IRQ_MODEM;
			last_wakeup = &wakeup_counts[wakeup_count_update_item];
			schedule_work(&wakeup_defer_work);
			break;
		}
	}
}
EXPORT_SYMBOL(scan_wakeup_irq);

/* This function run at interrupt disabled */
static int wakeup_monitor_suspend(void)
{
	ktime_t now;
	ktime_t awake_time;

	pr_debug("%s: Enter\n", __func__);

	if (likely(last_wakeup)) {
		now = ns_to_ktime(sched_clock());
		awake_time = ktime_sub(now, last_wakeup->last_time);
		/* calculated the total wakeup time */
		last_wakeup->total_time =
				ktime_add(last_wakeup->total_time, awake_time);
	}

	last_wakeup = NULL;
	wakeup_count_update_item = IRQ_UNKNOWN;
	return 0;
}

/*
static void wakeup_monitor_resume(void)
{
	wm_resumed_count++;
	pr_debug("%s: resume count = %lu\n", __func__, wm_resumed_count);
}
*/

static int wm_suspend_notifier(struct notifier_block *nb, unsigned long event,
				void *dummy)
{
#if defined(CONFIG_AMAZON_METRICS_LOG)
	char buff[64];
#endif
	if (enabled) {
		if (event == PM_SUSPEND_PREPARE) {
#if defined(CONFIG_AMAZON_METRICS_LOG)
			snprintf(buff, sizeof(buff),
				 "power:wakeup_mon:enter suspend:NR");
			log_to_metrics(ANDROID_LOG_INFO, "kernel", buff);
		}
#endif
	}
	return NOTIFY_DONE;
}

static struct syscore_ops wakeup_monitor_ops = {
	.suspend = wakeup_monitor_suspend,
	/* .resume = wakeup_monitor_resume, */
};

static struct notifier_block wakeup_monitor_pm_notifier = {
	.notifier_call = wm_suspend_notifier,
};

static int __init wakeup_monitor_init(void)
{
	struct proc_dir_entry *entry;

	memset(control_buff, 0, sizeof(BUFFER_LEN));
	strlcpy(control_buff, DEFAULT_CONTROL, strlen(DEFAULT_CONTROL)+1);

	proc_wakeup_monitor_dir = proc_mkdir("wakeup_monitor", NULL);
	if (!proc_wakeup_monitor_dir) {
		pr_err("%s: ERROR failed to mkdir wakeup_monitor dir\n",
		       __func__);
		return -ENOMEM;
	}

	entry = proc_create("control", S_IRUGO|S_IWUGO, proc_wakeup_monitor_dir,
			    &wakeup_proc_control_fops);
	if (!entry) {
		pr_err("%s: Error failed to create control entry\n", __func__);
		goto fail1;
	}

	entry = proc_create("modem_count", 0, proc_wakeup_monitor_dir,
			    &wakeup_proc_modem_fops);
	if (!entry) {
		pr_err("%s: Error failed to create modem entry\n", __func__);
		goto fail2;
	}

	entry = proc_create("wifi_count", 0, proc_wakeup_monitor_dir,
		    &wakeup_proc_wifi_fops);

	if (!entry) {
		pr_err("%s: Error failed to create wifi entry\n", __func__);
		goto fail3;
	}

	entry = proc_create("alarm_count", 0, proc_wakeup_monitor_dir,
		    &wakeup_proc_alarm_fops);
	if (!entry) {
		pr_err("%s: Error failed to create alarm entry\n", __func__);
		goto fail4;
	}

	/* registering PM notifier */
	register_pm_notifier(&wakeup_monitor_pm_notifier);

	INIT_WORK(&wakeup_defer_work, wakeup_defer_func);

	wakeup_source_init(&wakeup_monitor_ws, "wakeup_monitor");

	register_syscore_ops(&wakeup_monitor_ops);

	return 0;

fail4:	remove_proc_entry("wifi_count", proc_wakeup_monitor_dir);
fail3:	remove_proc_entry("modem_count", proc_wakeup_monitor_dir);
fail2:	remove_proc_entry("control", proc_wakeup_monitor_dir);
fail1:	remove_proc_entry("wakeup_monitor", NULL);

	return -ENOMEM;
}

module_init(wakeup_monitor_init);

static void wakeup_monitor_exit(void)
{
	remove_proc_entry("alarm_count", proc_wakeup_monitor_dir);
	remove_proc_entry("wifi_count", proc_wakeup_monitor_dir);
	remove_proc_entry("modem_count", proc_wakeup_monitor_dir);
	remove_proc_entry("control", proc_wakeup_monitor_dir);
	remove_proc_entry("wakeup_monitor", NULL);
}

module_exit(wakeup_monitor_exit);
