/*
 * wakeup_monitor.c - PM wakeup minitor functionality
 *
 * Copyright (c) 2014 Lab126
 *
 */
#include <linux/kobject.h>
#include <linux/sysfs.h>
#include <linux/proc_fs.h>
#include <asm/uaccess.h>
#include <linux/sched.h>
#include <linux/export.h>
#include <linux/rtc.h>
#include <linux/workqueue.h>
#include <linux/syscore_ops.h>
#include <linux/kernel.h>
#include <linux/slab.h>
#include <linux/cpu.h>
#include <linux/cpufreq.h>
#include <linux/module.h>
#include <linux/jiffies.h>
#include <linux/percpu.h>
#include <linux/spinlock.h>
#include <linux/notifier.h>
#include <asm/cputime.h>
#include <linux/printk.h>
#include <linux/suspend.h>

#include "linux/wm.h"
#include <linux/interrupt.h>
#include <linux/irq.h>

#if defined(CONFIG_AMAZON_METRICS_LOG)
#include <linux/metricslog.h>
#include <linux/rtc.h>
#endif

#define FILE_MODE 644
#define BUFFER_LEN 8
#define EVENT_STR_LEN 32
#define SEP_STR "!w!"
#define WIFI_EVENT_HEADER "WIFI_WAKUP="
#define MODEM_EVENT_HEADER "MODEM_WAKEUP="
struct proc_dir_entry *wm_proc_entry=NULL;
static char control_buff[BUFFER_LEN];


#define PWR_BUTTON_IRQ 291
#define WCNSS_TX 177
#define WCNSS_RX 178
#define SMD_MODEM 57
#define RTC_ALARM 326
#define RPM_IRQ 200
#define DEFAULT_CONTROL "0\0" /* enabled by default */

#define WAKEUP_CPU_OFFLINE 0
#define WAKEUP_CPU_ONLINE 1

enum wm_control_type {
	WM_CONTROL_TYPE_COUNTS,
	WM_CONTROL_TYPE_TCP,
	WM_CONTROL_TYPE_CPUFREQ,
	WM_CONTROL_TYPE_MAX
};

#define WM_CONTROL_SIZE (1)
#define WM_CONTROL_MASK_DEFAULT (0)
#define WM_CONTROL_MASK_COUNT (1U << WM_CONTROL_TYPE_COUNTS)
#define WM_CONTROL_MASK_TCP (1U << WM_CONTROL_TYPE_TCP)
#define WM_CONTROL_MASK_CPUFREQ (1U << WM_CONTROL_TYPE_CPUFREQ)

#define WM_CONTROL_LEVEL_0 (WM_CONTROL_MASK_DEFAULT)
#define WM_CONTROL_LEVEL_1 (WM_CONTROL_MASK_COUNT)
#define WM_CONTROL_LEVEL_2 (WM_CONTROL_MASK_COUNT | WM_CONTROL_MASK_TCP)
#define WM_CONTROL_LEVEL_3 (WM_CONTROL_MASK_COUNT | WM_CONTROL_MASK_TCP \
							| WM_CONTROL_MASK_CPUFREQ)

static int enabled = WM_CONTROL_LEVEL_0;		/* default is disabled */
static int wakeup_detected = 0;	/* default is not detect */
static struct work_struct wakeup_defer_work;

static spinlock_t wakeup_cpufreq_stats_lock;


enum irq_num {
	IRQ_UNKNOWN = 0,
	IRQ_WCNSS_RX,
	IRQ_MODEM,
	IRQ_RTC_ALARM,
	IRQ_TYPES,
};

typedef enum irq_num irq_num_t;

struct wakeup_event_monitor {
	irq_num_t event;
	unsigned long count;
	ktime_t last_time;
	ktime_t total_time;
};

#define MAX_PROCESS_NAME 64
#define MAX_PID_PRINT 3
struct wakeup_process {
	char name[MAX_PROCESS_NAME];
	pid_t pid;
	pid_t uid;
	unsigned long count;
	struct list_head list;
};

struct wakeup_cpufreq_stats {
	unsigned int cpu;
	unsigned int total_trans;
	unsigned long long last_time;
	unsigned int max_state;
	unsigned int state_num;
	unsigned int last_index;
	cputime64_t *time_in_state;
	unsigned int *freq_table;
	//unsigned int total_off_time; //maintain the total offline time per cpu
	unsigned int total_on_time;	//maintain the total online time per cpu, this should == sum of time@freq?
	unsigned int status;	// maintain the cpu status online/offline
};
static DEFINE_PER_CPU(struct wakeup_cpufreq_stats *,
		      wakeup_cpufreq_stats_table);

static LIST_HEAD(wifi_wakeup_processes_list);
static LIST_HEAD(alarm_wakeup_processes_list);
static LIST_HEAD(modem_wakeup_processes_list);

//static enum irq_num last_triggered_irq = IRQ_UNKNOWN;
//static unsigned long wakeup_counts[IRQ_TYPES];
static struct wakeup_event_monitor wakeup_counts[IRQ_TYPES];
static struct wakeup_event_monitor *last_wakeup = NULL;
static unsigned long wakeup_uevent_trigger_counts[IRQ_TYPES];
static unsigned long exit_vmin_count=0;
static enum irq_num wakeup_count_update_item=IRQ_UNKNOWN;
static struct kobject *wm_root = NULL;
static struct kobject *wm_kobj = NULL;
static struct kset *wm_kset = NULL;
static u32 last_vmin_count = 0;
static int print_pid = 0;

static struct wakeup_source wakeup_monitor_ws;

#define DEFINE_OPEN_FUNC(name) proc_##name##_open

#define DEFINE_FILE_OPERATIONS(name)                                    \
                                                                        \
        struct file_operations wakeup_proc_##name##_fops = {            \
                .open = DEFINE_OPEN_FUNC(name),                         \
                .read = seq_read,                                       \
                .llseek = seq_lseek,                                    \
                .release = single_release,                              \
}

#define DEFINE_PROC_OPEN(name)                                          \
                                                                        \
        int proc_##name##_open(struct inode *inode, struct file *filp) { \
                return single_open(filp, show_##name, NULL);            \
        }

static int wakeup_process_alloc_and_add(struct list_head *head,
					const char *name, 
					const pid_t pid, 
					const pid_t uid)
{
	struct wakeup_process *wk =
	    kzalloc(sizeof(struct wakeup_process), GFP_KERNEL);

	if (!wk) {
		pr_err("%s: Error failed to allocate memory for wakeup_process\n",
		     __func__);
		return -ENOMEM;
	}

	strncpy(wk->name, name, MAX_PROCESS_NAME - 1);
	wk->pid = pid;
	wk->uid = uid;
	wk->count = 1;
	INIT_LIST_HEAD(&wk->list);
	list_add(&wk->list, head);

	return 0;
}

static void wakeup_process_show(struct seq_file *m, struct list_head *head)
{
	struct wakeup_process *wk, *t;
	list_for_each_entry_safe(wk, t, head, list) {
		seq_printf(m, "%s:%lu\n", wk->name, wk->count);
	}
}

static int wakeup_process_find_and_increment(struct list_head *head, const int uid)
{
	struct wakeup_process *wk, *t;
	list_for_each_entry_safe(wk, t, head, list) {
		if (wk->uid == uid) {
			pr_debug("%s: found the process=%s with uid=%d, count=%lu\n",
			     __func__, wk->name, uid, wk->count);
			wk->count++;
			return 1;
		}
	}
	return 0;
}

#if defined(WAKEUP_MONITOR_CHECK_KOBJ)
static void check_kobj(void)
{
	struct kobject *top_kobj;
	const char *devpath = NULL;

	top_kobj = wm_kobj;
	while (!top_kobj->kset && top_kobj->parent)
		top_kobj = top_kobj->parent;
	if (!top_kobj->kset) {
		pr_err("%s: ERROR '%s' (%p): attempted to send uevent "
		       "without kset!\n", __func__, kobject_name(wm_kobj),
		       wm_kobj);
	} else {
		pr_info("%s: top_kobj is not null. %s attempt to send uevent\n",
			__func__, kobject_name(wm_kobj));

		devpath = kobject_get_path(wm_kobj, GFP_KERNEL);
		if (!devpath) {
			pr_err("%s: devpath is not allocated.\n", __func__);
		} else {
			pr_info("%s: subsystem = %s, dev_path = %s\n",
				__func__, kobject_name(&wm_kset->kobj),
				devpath);
			kfree(devpath);
		}

	}
}
#endif

#if defined (WAKEUP_MONITOR_PRINT_TIMER)
static void print_timer(void)
{
	struct timespec ts;
//	struct rtc_time tm;
	getnstimeofday(&ts);

	if (!first_wakeup) {
		struct timespec diff = timespec_sub(ts, last_wakeup_time);
		pr_info("%s: rtc time diff %lu(sec) and %lu(nsec)\n",
			__func__, diff.tv_sec, diff.tv_nsec);
	} else {
		/* first wakeup */
		first_wakeup = 0;
		//check_kobj();
	}

	last_wakeup_time.tv_sec = ts.tv_sec;
	last_wakeup_time.tv_nsec = ts.tv_nsec;

#if 0
	rtc_time_to_tm(ts.tv_sec, &tm);
	pr_info(" %d-%02d-%02d %02d:%02d:%02d.%09lu UTC\n",
		tm.tm_year + 1900, tm.tm_mon + 1, tm.tm_mday,
		tm.tm_hour, tm.tm_min, tm.tm_sec, ts.tv_nsec);
#endif
}

#endif

static ssize_t control_write(struct file *file, const char __user * buffer,
			     size_t len, loff_t * pos)
{
	/* len -1 to assure the null-terminator character */
	ssize_t cp_len = len > (BUFFER_LEN - 1) ? (BUFFER_LEN - 1) : len;
	// buffer will also include the space is "echo enable > " with space and
	// the len is 7
	memset(control_buff, 0, BUFFER_LEN);
	if (copy_from_user(control_buff, buffer, cp_len)) {
		return -EFAULT;
	}

	if (strncmp(control_buff, "0", WM_CONTROL_SIZE) == 0) {
		enabled = WM_CONTROL_LEVEL_0;
	} else if (strncmp(control_buff, "1", WM_CONTROL_SIZE) == 0) {
		enabled = WM_CONTROL_LEVEL_1;
	} else if (strncmp(control_buff, "2", WM_CONTROL_SIZE) == 0){
		enabled = WM_CONTROL_LEVEL_2;
	} else if (strncmp(control_buff, "3", WM_CONTROL_SIZE) == 0){
		enabled = WM_CONTROL_LEVEL_3;
	} else {
		enabled = WM_CONTROL_LEVEL_0;
	}
	
	return cp_len;
}

//static int wm_read_proc(char *page, char **start,
//              off_t off,int count, int *eof, void *data)
static int show_control(struct seq_file *m, void *v)
{
	int len = strlen(control_buff);
	if (len <= 0) {
		pr_err("%s: ERROR control_buff string is invalid\n", __func__);
		return -EBUSY;
	}

	seq_printf(m, "%s\n", control_buff);
	return 0;
}

/* 
   wifi/alarm/modem sysfs format:
   wakeup_counts uevent_trigger_counts
   locking is not necessary since the scan_wakeup_irq is invoked with local irq disabled.
   do we need spin lock? do we care race condition?
 */
static int show_wifi(struct seq_file *m, void *v)
{
	seq_puts(m, "wakeup_count\tawake_time\tuevent_count\n");
	seq_printf(m, "%lu\t%lld\t%lu\n", wakeup_counts[IRQ_WCNSS_RX].count,
		ktime_to_ms(wakeup_counts[IRQ_WCNSS_RX].total_time),
		wakeup_uevent_trigger_counts[IRQ_WCNSS_RX]);

	wakeup_process_show(m, &wifi_wakeup_processes_list);
	return 0;
}

static int show_alarm(struct seq_file *m, void *v)
{
	seq_puts(m, "wakeup_count\tawake_time\tuevent_count\n");
	seq_printf(m, "%lu\t%lld\t%lu\n", wakeup_counts[IRQ_RTC_ALARM].count,
		ktime_to_ms(wakeup_counts[IRQ_RTC_ALARM].total_time),
		wakeup_uevent_trigger_counts[IRQ_RTC_ALARM]);
	wakeup_process_show(m, &alarm_wakeup_processes_list);
	return 0;
}

static int show_modem(struct seq_file *m, void *v)
{
	seq_puts(m, "wakeup_count\tawake_time\tuevent_count\n");
	seq_printf(m, "%lu\t%lld\t%lu\n", wakeup_counts[IRQ_MODEM].count,
		ktime_to_ms(wakeup_counts[IRQ_MODEM].total_time),
		wakeup_uevent_trigger_counts[IRQ_MODEM]);
	return 0;
}

static int show_vmin(struct seq_file *m, void *v)
{
        seq_printf(m, "%lu\n", exit_vmin_count);
        return 0;
}

static int wakeup_cpufreq_stats_update(unsigned int cpu)
{
	struct wakeup_cpufreq_stats *stat;
	unsigned long long cur_time;

	if (enabled & WM_CONTROL_MASK_CPUFREQ) {
        cur_time = get_jiffies_64();
        spin_lock(&wakeup_cpufreq_stats_lock);
        stat = per_cpu(wakeup_cpufreq_stats_table, cpu);
        if (!stat) {
			spin_unlock(&wakeup_cpufreq_stats_lock);
			return 0;
		}

        if (stat->time_in_state)
			stat->time_in_state[stat->last_index] +=
	            cur_time - stat->last_time;

		stat->last_time = cur_time;
		spin_unlock(&wakeup_cpufreq_stats_lock);
	}
	return 0;
}

static int show_cpu0(struct seq_file *m, void *v)
{
	int i;
	struct wakeup_cpufreq_stats *stat =
	    per_cpu(wakeup_cpufreq_stats_table, 0);
	if (!stat)
		return 0;
	wakeup_cpufreq_stats_update(stat->cpu);
	for (i = 0; i < stat->state_num; i++) {
		seq_printf(m, "%u %llu\n", stat->freq_table[i],
			   (unsigned long long)
			   cputime64_to_clock_t(stat->time_in_state[i]));
	}
	return 0;
}

static int show_cpu1(struct seq_file *m, void *v)
{
	int i;
	struct wakeup_cpufreq_stats *stat =
	    per_cpu(wakeup_cpufreq_stats_table, 1);

	if (!stat)
		return 0;
	if (stat->status == WAKEUP_CPU_ONLINE)
		wakeup_cpufreq_stats_update(stat->cpu);
	for (i = 0; i < stat->state_num; i++) {
		seq_printf(m, "%u %llu\n", stat->freq_table[i],
			   (unsigned long long)
			   cputime64_to_clock_t(stat->time_in_state[i]));
	}
	return 0;
}

static int show_cpu2(struct seq_file *m, void *v)
{
	int i;
	struct wakeup_cpufreq_stats *stat =
	    per_cpu(wakeup_cpufreq_stats_table, 2);

	if (!stat)
		return 0;
	if (stat->status == WAKEUP_CPU_ONLINE)
		wakeup_cpufreq_stats_update(stat->cpu);
	for (i = 0; i < stat->state_num; i++) {
		seq_printf(m, "%u %llu\n", stat->freq_table[i],
			   (unsigned long long)
			   cputime64_to_clock_t(stat->time_in_state[i]));
	}
	return 0;
}

static int show_cpu3(struct seq_file *m, void *v)
{
	int i;
	struct wakeup_cpufreq_stats *stat =
	    per_cpu(wakeup_cpufreq_stats_table, 3);

	if (!stat)
		return 0;
	if (stat->status == WAKEUP_CPU_ONLINE)
		wakeup_cpufreq_stats_update(stat->cpu);
	for (i = 0; i < stat->state_num; i++) {
		seq_printf(m, "%u %llu\n", stat->freq_table[i],
			   (unsigned long long)
			   cputime64_to_clock_t(stat->time_in_state[i]));
	}
	return 0;
}


#ifdef CONFIG_WAKEUP_MONITOR_UEVENT
static int trigger_uevent(char *str)
{
	char *envp[2];
	int ret = 0;
	if (!str) {
		pr_err("%s: Error invalid pointer.\n", __func__);
		return 1;
	}

	envp[0] = str;
	envp[1] = NULL;

	pr_debug("%s: sending uevent: %s\n", __func__, str);
	ret = kobject_uevent_env(wm_kobj, KOBJ_CHANGE, envp);
	if (ret) {
		pr_err("%s: Error failed to send uevent: %s\n", __func__, str);
	}

	return ret;
}
#else
#define trigger_uevent(a)
#endif

extern int msm_rpmstats_read(u32 *);

static int is_wakeup_from_vmin(void)
{
	u32 vmin_count=0;   /* tracking the last vmin counter */
	pr_debug("%s: Enter(cpu=%u, pid=%d)\n", __func__, smp_processor_id(), current->pid);
        /* every wakeup read the count */
        if (msm_rpmstats_read(&vmin_count) == 0){
                if (vmin_count > last_vmin_count){
                        pr_info("%s: wakeup from vmin\n",  __func__);
                        last_vmin_count = vmin_count;
			exit_vmin_count++;
			return 1;
		}
	}
	return 0;
}

/*  
    Notes:
    we possibly can't send uevent to user space in the syscore_ops function
    since the userspace already frozen. so we do it in the defered function

    it is possible that we are in the middle of the function, the autosleep
    thread start again. maybe take a wake lock and release it.

    There is possible race that when we update the trigger count, the
    userspace process is reading the alarm count, but it is ok, since the
    userspace is just reading the count, it does not really matter the count
    is getting updated while in the middle of reading.

    another race condition is that the system enter suspend again in the middle
    __pm_stay_awake() function, which is ok, we allow the system enter suspend \
    before we update the trigger counts and sending the uevent, we just missed
    one count, not bit deal. Since it is likely that the system enter suspend
    event before the wakeup_defer_func() get chance (scheduled) to run.
 */
static void wakeup_defer_func(struct work_struct *work)
{
#ifdef CONFIG_AMAZON_METRICS_LOG
	char metrics_buffer[128];
#endif
	int woken_from_vmin = 0;
	// hold a wake lock just to make sure we send the uevent
	__pm_stay_awake(&wakeup_monitor_ws); 
	if (wakeup_count_update_item !=IRQ_UNKNOWN) {

		if (wakeup_count_update_item == IRQ_RTC_ALARM){
			wakeup_uevent_trigger_counts[IRQ_RTC_ALARM]++;

			trigger_uevent("ALARM_WAKEUP=TRIGGERED");
			pr_debug("%s: cur_pid=%d,tgid=%d,g_pid=%d,g_tgid=%d,cur=%s,g_cur=%s, uid=%d\n",
				 __func__, current->pid, current->tgid,
				 current->group_leader->pid,
				 current->group_leader->tgid,
				 current->comm,
				 current->group_leader->comm,
				 current_uid());
			if (!wakeup_process_find_and_increment(&alarm_wakeup_processes_list, current_uid())){
				/* did not find the process, alloc a new one and add it */
				wakeup_process_alloc_and_add(&alarm_wakeup_processes_list, current->group_leader->comm,
						     current->pid, current_uid());
			}
		}
		woken_from_vmin = is_wakeup_from_vmin();
	}
	__pm_relax(&wakeup_monitor_ws);

#ifdef CONFIG_AMAZON_METRICS_LOG
	snprintf(metrics_buffer,sizeof(metrics_buffer),
		 "power:wakeup_mon:wk_type=%d;DV;1,wk_vmin=%d;CT;1:NR",
		 wakeup_count_update_item, woken_from_vmin);
	log_to_metrics(ANDROID_LOG_INFO, "kernel", metrics_buffer);

#endif
}

void scan_wakeup_irq(unsigned int irq_num)
{
	if (enabled & WM_CONTROL_MASK_COUNT) {
		switch (irq_num) {
		case WCNSS_RX:
			pr_debug("%s: wifi interrupt triggered\n", __func__);
			wakeup_detected = 1;
			wakeup_count_update_item=IRQ_WCNSS_RX;
			wakeup_counts[wakeup_count_update_item].count++;
			wakeup_counts[wakeup_count_update_item].last_time = ns_to_ktime(sched_clock());	/* note down the last wakeup time */
			wakeup_counts[wakeup_count_update_item].event = IRQ_WCNSS_RX;
			last_wakeup = &wakeup_counts[wakeup_count_update_item];
			print_pid=MAX_PID_PRINT;
			schedule_work(&wakeup_defer_work);
			break;
		case RTC_ALARM:
			pr_debug("%s: alarm interrupt triggered\n", __func__);
			wakeup_count_update_item = IRQ_RTC_ALARM;
			wakeup_detected = 0; /* clear the flag just incase alarm irq followed by wifi  */
			wakeup_counts[wakeup_count_update_item].count++;
			wakeup_counts[wakeup_count_update_item].last_time = ns_to_ktime(sched_clock()); /* note down the last wakeup time */
			wakeup_counts[wakeup_count_update_item].event = IRQ_RTC_ALARM;
			last_wakeup = &wakeup_counts[wakeup_count_update_item];
			schedule_work(&wakeup_defer_work);
			break;
		case SMD_MODEM:
			pr_debug("%s: modem interrupt triggered\n", __func__);
			wakeup_detected = 1;
			wakeup_count_update_item = IRQ_MODEM;
			wakeup_counts[wakeup_count_update_item].count++;
			wakeup_counts[wakeup_count_update_item].last_time = ns_to_ktime(sched_clock()); /* note down the last wakeup time */
			wakeup_counts[wakeup_count_update_item].event = IRQ_MODEM;
			last_wakeup = &wakeup_counts[wakeup_count_update_item];
			print_pid=MAX_PID_PRINT;
			schedule_work(&wakeup_defer_work);
			break;
		}
	}
}

EXPORT_SYMBOL(scan_wakeup_irq);

void reset_wakeup_detected(void)
{
	if (print_pid > 0) {
		pr_info("%s: cur_pid = %d, %s, %s\n", __func__,
			current->pid, current->comm,
			current->group_leader->comm);
		print_pid--;
	}
	wakeup_detected = 0;
}

EXPORT_SYMBOL(reset_wakeup_detected);

void check_wakeup(void)
{
	char str[EVENT_STR_LEN];

	if (enabled & WM_CONTROL_MASK_TCP) {
		if (print_pid > 0) {
			pr_info("%s: Enter, cur_pid = %d, %s, %s\n", __func__,
				current->pid, current->comm,
				current->group_leader->comm);
			print_pid--;
		}

		if (wakeup_detected) {
			/* remote wakeup is detected */
			pr_info("%s: WIFI Wakeup: process id: %d, group leader pid: %d,"			\
			       "process name: %s, grounp leader/Application name: %s\n",
			       __func__, current->pid, current->tgid, 
			       current->comm, current->group_leader->comm);

			if (wakeup_count_update_item == IRQ_WCNSS_RX) {
				snprintf(str, EVENT_STR_LEN - 1, "%s%s%s%d",
					 WIFI_EVENT_HEADER, current->group_leader->comm,
					 SEP_STR, current_uid());
				pr_debug("%s: wifi uevent triggered\n", __func__);
				wakeup_uevent_trigger_counts[IRQ_WCNSS_RX]++;
				
				if (!wakeup_process_find_and_increment(&wifi_wakeup_processes_list, current_uid())){
					/* did not find the process, alloc a new one and add it */
					wakeup_process_alloc_and_add(&wifi_wakeup_processes_list, current->group_leader->comm,
								     current->pid, current_uid());
				}
				trigger_uevent(str);
			}else if (wakeup_count_update_item == IRQ_MODEM){
				snprintf(str, EVENT_STR_LEN - 1, "%s%s%s%d", 
					 MODEM_EVENT_HEADER, 
					 current->group_leader->comm, SEP_STR, current_uid());
				pr_debug("%s: modem uevent triggered\n", __func__);
				wakeup_uevent_trigger_counts[IRQ_MODEM]++;

				if (!wakeup_process_find_and_increment(&modem_wakeup_processes_list, current_uid())){
					/* did not find the process, alloc a new one and add it */
					wakeup_process_alloc_and_add(&modem_wakeup_processes_list, current->group_leader->comm,
								     current->pid, current_uid());
				}
				trigger_uevent(str);
			}
			/* clear the flag */
			reset_wakeup_detected();
			wakeup_count_update_item = IRQ_UNKNOWN;
		}
	}

}

EXPORT_SYMBOL(check_wakeup);

bool is_rtc_alarm_wakeup(void)
{
	if (likely(last_wakeup))
		return (last_wakeup->event == IRQ_RTC_ALARM)?true:false;

	return false;
}
EXPORT_SYMBOL(is_rtc_alarm_wakeup);

static unsigned long flag = 1;
static ssize_t wm_show(struct kobject *kobj, 
		       struct attribute *attr, char *buf)
{
	return sprintf(buf, "%lu\n", flag);
}

static struct proc_dir_entry *proc_wakeup_monitor_dir = NULL;

static DEFINE_PROC_OPEN(control)
static DEFINE_PROC_OPEN(wifi)
static DEFINE_PROC_OPEN(alarm)
static DEFINE_PROC_OPEN(modem)
static DEFINE_PROC_OPEN(vmin)

static DEFINE_PROC_OPEN(cpu0)
static DEFINE_PROC_OPEN(cpu1)
static DEFINE_PROC_OPEN(cpu2)
static DEFINE_PROC_OPEN(cpu3)

static const DEFINE_FILE_OPERATIONS(cpu0);
static const DEFINE_FILE_OPERATIONS(cpu1);
static const DEFINE_FILE_OPERATIONS(cpu2);
static const DEFINE_FILE_OPERATIONS(cpu3);

static const DEFINE_FILE_OPERATIONS(modem);
static const DEFINE_FILE_OPERATIONS(wifi);
static const DEFINE_FILE_OPERATIONS(alarm);
static const DEFINE_FILE_OPERATIONS(vmin);

static const struct file_operations wakeup_proc_control_fops = {
	.open = proc_control_open,
	.read = seq_read,
	.write = control_write,
	.llseek = seq_lseek,
	.release = single_release,
};

static struct attribute wm_attr = {
	.name = "wm",
	.mode = 0777,
};

static const struct sysfs_ops wm_ops = {
	.show = wm_show,
	.store = NULL,
};

static struct kobj_type wm_ktype = {
	.sysfs_ops = &wm_ops,
};

/* This function run at interrupt disabled */
static int wakeup_monitor_suspend(void)
{
	ktime_t now;
	ktime_t awake_time;

	pr_debug("%s: Enter\n", __func__);

	if (likely(last_wakeup)) {
		now = ns_to_ktime(sched_clock()); 
		awake_time = ktime_sub(now, last_wakeup->last_time);
		last_wakeup->total_time =
		        ktime_add(last_wakeup->total_time, awake_time);	/* calculated the total wakeup time */
	}
	last_wakeup = NULL;
	
	wakeup_detected = 0;
	wakeup_count_update_item = IRQ_UNKNOWN;
	print_pid = 0;
	return 0;
}

/*
static void wakeup_monitor_resume(void)
{
	wm_resumed_count++;
	pr_debug("%s: resume count = %lu\n", __func__, wm_resumed_count);
}
*/

static int wm_suspend_notifier(struct notifier_block *nb, unsigned long event,
			       void *dummy)
{
#if defined(CONFIG_AMAZON_METRICS_LOG)
	char buff[64];
#endif
	if (enabled & WM_CONTROL_MASK_COUNT) {
		if (event == PM_SUSPEND_PREPARE){
#if defined(CONFIG_AMAZON_METRICS_LOG)
			snprintf(buff,sizeof(buff),
				 "power:wakeup_mon:enter suspend:NR");
			log_to_metrics(ANDROID_LOG_INFO, "kernel", buff);
		}
#endif		
	}
	return NOTIFY_DONE;
}

static struct syscore_ops wakeup_monitor_ops = {
	.suspend = wakeup_monitor_suspend,
//	.resume = wakeup_monitor_resume,
};


static struct notifier_block wakeup_monitor_pm_notifier = {
	.notifier_call = wm_suspend_notifier,
};

static int wakeup_monitor_create_uevent_entry(struct kobject *parent)
{
	int err;

	/* instantiate a kset object */
	wm_kset = kset_create_and_add("wm_kset", NULL, parent);
	if (!wm_kset) {
		pr_err("%s: Error failed to create wm_kset object\n", __func__);
		return -ENOMEM;
	}
	wm_kobj = kzalloc(sizeof(struct kobject), GFP_KERNEL);
	if (!wm_kobj) {
		pr_err("%s: Error failed to create wm_kobj object\n", __func__);
		goto fail1;
	}

	wm_kobj->kset = wm_kset;
	err = kobject_init_and_add(wm_kobj, &wm_ktype, NULL, "wm_uevent");
	if (err) {
		pr_err("%s: ERROR failed to create 'wm_uevent' object\n",
		       __func__);
		goto fail2;
	}

	err = sysfs_create_file(wm_kobj, &wm_attr);

	if (err) {
		pr_err("%s: Error failed to create attributes\n", __func__);
		goto fail2;
	}

	return 0;

fail2: kobject_del(wm_kobj);
fail1: kset_unregister(wm_kset);
	return -ENOMEM;
}

static int wakeup_freq_table_get_index(struct wakeup_cpufreq_stats *stat,
				       unsigned int freq)
{
	int index;
	for (index = 0; index < stat->max_state; index++)
		if (stat->freq_table[index] == freq)
			return index;
	return -1;
}

static int wakeup_cpufreq_stats_create_table(struct cpufreq_policy *policy,
					     struct cpufreq_frequency_table
					     *table)
{
	unsigned int i, j, count = 0, ret = 0;
	struct wakeup_cpufreq_stats *stat;
	unsigned int alloc_size;
	unsigned int cpu = policy->cpu;

	if (per_cpu(wakeup_cpufreq_stats_table, cpu)) {
		return 0;
	}

	stat = kzalloc(sizeof(struct wakeup_cpufreq_stats), GFP_KERNEL);
	if ((stat) == NULL)
		return -ENOMEM;

	stat->cpu = cpu;
	stat->status = WAKEUP_CPU_ONLINE;
	per_cpu(wakeup_cpufreq_stats_table, cpu) = stat;

	for (i = 0; table[i].frequency != CPUFREQ_TABLE_END; i++) {
		unsigned int freq = table[i].frequency;
		if (freq == CPUFREQ_ENTRY_INVALID)
			continue;
		count++;
	}

	alloc_size = count * sizeof(int) + count * sizeof(cputime64_t);

	stat->max_state = count;
	stat->time_in_state = kzalloc(alloc_size, GFP_KERNEL);
	if (!stat->time_in_state) {
		ret = -ENOMEM;
		goto error_out;
	}
	stat->freq_table = (unsigned int *)(stat->time_in_state + count);

	j = 0;
	for (i = 0; table[i].frequency != CPUFREQ_TABLE_END; i++) {
		unsigned int freq = table[i].frequency;
		if (freq == CPUFREQ_ENTRY_INVALID)
			continue;
		if (wakeup_freq_table_get_index(stat, freq) == -1)
			stat->freq_table[j++] = freq;
	}
	stat->state_num = j;
	spin_lock(&wakeup_cpufreq_stats_lock);
	stat->last_time = get_jiffies_64();
	stat->last_index = wakeup_freq_table_get_index(stat, policy->cur);
	spin_unlock(&wakeup_cpufreq_stats_lock);
	return 0;
error_out:
	kfree(stat);
	per_cpu(wakeup_cpufreq_stats_table, cpu) = NULL;
	return ret;
}

static int wakeup_cpufreq_stat_notifier_policy(struct notifier_block *nb,
					       unsigned long val, void *data)
{
	int ret;
	struct cpufreq_policy *policy = data;
	struct cpufreq_frequency_table *table;
	unsigned int cpu = policy->cpu;
	//unsigned int i;

	if (val != CPUFREQ_NOTIFY)
		return 0;
	table = cpufreq_frequency_get_table(cpu);
	if (!table)
		return 0;

	//for (i = 0; table[i].frequency != CPUFREQ_TABLE_END; i++) {
	//      unsigned int freq = table[i].frequency;
	//      printk(KERN_WARNING" %d:\n", table[i].frequency);
	//      if (freq == CPUFREQ_ENTRY_INVALID)
	//              continue;
	//}

	ret = wakeup_cpufreq_stats_create_table(policy, table);
	if (ret)
		return ret;
	return 0;
}

static int wakeup_cpufreq_stat_notifier_trans(struct notifier_block *nb,
					      unsigned long val, void *data)
{
	struct cpufreq_freqs *freq = data;
	struct wakeup_cpufreq_stats *stat;
	int old_index, new_index;

    if (enabled & WM_CONTROL_MASK_CPUFREQ) {
	    if (val != CPUFREQ_POSTCHANGE)
		    return 0;

	    stat = per_cpu(wakeup_cpufreq_stats_table, freq->cpu);
	    if (!stat)
		    return 0;
	    if (stat->status == WAKEUP_CPU_OFFLINE) {
		    stat->status = WAKEUP_CPU_ONLINE;
		    spin_lock(&wakeup_cpufreq_stats_lock);
		    stat->last_time = get_jiffies_64();
		    spin_unlock(&wakeup_cpufreq_stats_lock);
		    // should return here?
	    }
	    old_index = stat->last_index;
	    new_index = wakeup_freq_table_get_index(stat, freq->new);

	    if (old_index == -1 || new_index == -1)
		    return 0;

	    wakeup_cpufreq_stats_update(freq->cpu);

	    if (old_index == new_index)
		    return 0;

	    spin_lock(&wakeup_cpufreq_stats_lock);
	    stat->last_index = new_index;
	    stat->total_trans++;
	    spin_unlock(&wakeup_cpufreq_stats_lock);
    }
	return 0;
}

static int __cpuinit wakeup_cpufreq_stat_cpu_callback(struct notifier_block
						      *nfb,
						      unsigned long action,
						      void *hcpu)
{
	unsigned int cpu = (unsigned long)hcpu;
	struct wakeup_cpufreq_stats *stat =
	    per_cpu(wakeup_cpufreq_stats_table, cpu);

    if (enabled & WM_CONTROL_MASK_CPUFREQ) {
	    switch (action) {
	    case CPU_DEAD:
	    case CPU_DEAD_FROZEN:
		    if (stat) {
			    // update the stats for the last freq before offline
			    wakeup_cpufreq_stats_update(cpu);
			    stat->status = WAKEUP_CPU_OFFLINE;
		    }
		    break;
	    }
	}
	return NOTIFY_OK;
}

/* should be called late in the CPU removal sequence so that the stats
 * memory is still available in case someone tries to use it.
 */
static void wakeup_cpufreq_stats_free_table(unsigned int cpu)
{
	struct wakeup_cpufreq_stats *stat =
	    per_cpu(wakeup_cpufreq_stats_table, cpu);
	if (stat) {
		kfree(stat->time_in_state);
		kfree(stat);
	}
	per_cpu(wakeup_cpufreq_stats_table, cpu) = NULL;
}

static struct notifier_block wakeup_cpufreq_stat_cpu_notifier __refdata = {
	.notifier_call = wakeup_cpufreq_stat_cpu_callback
};

static struct notifier_block wakeup_notifier_policy_block = {
	.notifier_call = wakeup_cpufreq_stat_notifier_policy
};

static struct notifier_block wakeup_notifier_trans_block = {
	.notifier_call = wakeup_cpufreq_stat_notifier_trans
};

static int wakeup_cpufreq_stats_init(void)
{
	int ret;
	unsigned int cpu;

	spin_lock_init(&wakeup_cpufreq_stats_lock);
	ret = cpufreq_register_notifier(&wakeup_notifier_policy_block,
					CPUFREQ_POLICY_NOTIFIER);
	if (ret)
		return ret;

	ret = cpufreq_register_notifier(&wakeup_notifier_trans_block,
					CPUFREQ_TRANSITION_NOTIFIER);
	if (ret) {
		cpufreq_unregister_notifier(&wakeup_notifier_policy_block,
					    CPUFREQ_POLICY_NOTIFIER);
		return ret;
	}

	register_hotcpu_notifier(&wakeup_cpufreq_stat_cpu_notifier);

	for_each_online_cpu(cpu) {
		cpufreq_update_policy(cpu);
	}
	return 0;
}

static void wakeup_cpufreq_stats_exit(void)
{
	unsigned int cpu;

	cpufreq_unregister_notifier(&wakeup_notifier_policy_block,
				    CPUFREQ_POLICY_NOTIFIER);
	cpufreq_unregister_notifier(&wakeup_notifier_trans_block,
				    CPUFREQ_TRANSITION_NOTIFIER);
	unregister_hotcpu_notifier(&wakeup_cpufreq_stat_cpu_notifier);
	for_each_possible_cpu(cpu) {
		printk(KERN_WARNING " wakeup_cpufreq_stats_exit cpu:%d\n", cpu);
		wakeup_cpufreq_stats_free_table(cpu);
	}
}

//int init_wakeup_monitor(struct kobject *parent)
static int __init wakeup_monitor_init(void)
{
	int err;
	struct proc_dir_entry *entry;
	memset(control_buff, 0, sizeof(BUFFER_LEN));

	strncpy(control_buff, DEFAULT_CONTROL, strlen(DEFAULT_CONTROL));
	// create node /sys/wakeup_monitor
	wm_root = kobject_create_and_add("wakeup_monitor", NULL);

	if (!wm_root) {
		pr_err("%s: Error failed to create wm_root object\n", __func__);
		return -ENOMEM;
	}

    proc_wakeup_monitor_dir = proc_mkdir("wakeup_monitor", NULL);
    if (!proc_wakeup_monitor_dir){
            pr_err("%s: ERROR failed to mkdir wakeup_monitor dir\n", __func__);
            return -ENOMEM;
    }

	entry = proc_create("control", 0, proc_wakeup_monitor_dir,
			    &wakeup_proc_control_fops);
	if (!entry) {
		pr_err("%s: Error failed to create control entry\n", __func__);
		goto fail1;
	}

	entry = proc_create("modem_count", 0, proc_wakeup_monitor_dir,
			    &wakeup_proc_modem_fops);
	if (!entry) {
		pr_err("%s: Error failed to create modem entry\n", __func__);
		goto fail2;
	}

	proc_create("wifi_count", 0, proc_wakeup_monitor_dir,
		    &wakeup_proc_wifi_fops);

	if (!entry) {
		pr_err("%s: Error failed to create wifi entry\n", __func__);
		goto fail3;
	}

	proc_create("alarm_count", 0, proc_wakeup_monitor_dir,
		    &wakeup_proc_alarm_fops);
	if (!entry) {
		pr_err("%s: Error failed to create alarm entry\n", __func__);
		goto fail4;
	}

	proc_create("cpu0_time_in_freq", 0, proc_wakeup_monitor_dir,
		    &wakeup_proc_cpu0_fops);
	if (!entry) {
		pr_err("%s: Error failed to create cpu0 entry\n", __func__);
		goto fail5;
	}
	proc_create("cpu1_time_in_freq", 0, proc_wakeup_monitor_dir,
		    &wakeup_proc_cpu1_fops);
	if (!entry) {
		pr_err("%s: Error failed to create cpu1 entry\n", __func__);
		goto fail6;
	}
	proc_create("cpu2_time_in_freq", 0, proc_wakeup_monitor_dir,
		    &wakeup_proc_cpu2_fops);
	if (!entry) {
		pr_err("%s: Error failed to create cpu2 entry\n", __func__);
		goto fail7;
	}
	proc_create("cpu3_time_in_freq", 0, proc_wakeup_monitor_dir,
		    &wakeup_proc_cpu3_fops);
	if (!entry) {
		pr_err("%s: Error failed to create cpu3 entry\n", __func__);
		goto fail8;
	}

	/* registering PM notifier */
	register_pm_notifier(&wakeup_monitor_pm_notifier);

	INIT_WORK(&wakeup_defer_work, wakeup_defer_func);

	wakeup_source_init(&wakeup_monitor_ws, "wakeup_monitor");

	err = wakeup_monitor_create_uevent_entry(wm_root);

	if (err)
		goto fail9;

	wakeup_cpufreq_stats_init();

	register_syscore_ops(&wakeup_monitor_ops);

	return 0;

fail9:	remove_proc_entry("cpu3_time_in_freq", proc_wakeup_monitor_dir);
fail8:	remove_proc_entry("cpu2_time_in_freq", proc_wakeup_monitor_dir);
fail7:	remove_proc_entry("cpu1_time_in_freq", proc_wakeup_monitor_dir);
fail6:	remove_proc_entry("cpu0_time_in_freq", proc_wakeup_monitor_dir);
fail5:	remove_proc_entry("alarm_count", proc_wakeup_monitor_dir);
fail4:	remove_proc_entry("wifi_count", proc_wakeup_monitor_dir);
fail3:	remove_proc_entry("modem_count", proc_wakeup_monitor_dir);
fail2:	remove_proc_entry("control", proc_wakeup_monitor_dir);
fail1:	remove_proc_entry("wakeup_monitor", NULL);

	kobject_del(wm_root);

	return -ENOMEM;
}

module_init(wakeup_monitor_init);

static void wakeup_monitor_exit(void)
{
	wakeup_cpufreq_stats_exit();
	sysfs_remove_file(wm_kobj, &wm_attr);
	kset_unregister(wm_kset);
	kobject_del(wm_kobj);
	kobject_del(wm_root);

	remove_proc_entry("cpu3_time_in_freq", proc_wakeup_monitor_dir);
	remove_proc_entry("cpu2_time_in_freq", proc_wakeup_monitor_dir);
	remove_proc_entry("cpu1_time_in_freq", proc_wakeup_monitor_dir);
	remove_proc_entry("cpu0_time_in_freq", proc_wakeup_monitor_dir);
	remove_proc_entry("alarm_count", proc_wakeup_monitor_dir);
	remove_proc_entry("wifi_count", proc_wakeup_monitor_dir);
	remove_proc_entry("modem_count", proc_wakeup_monitor_dir);
	remove_proc_entry("control", proc_wakeup_monitor_dir);

	remove_proc_entry("wakeup_monitor", NULL);
}

module_exit(wakeup_monitor_exit);
//MODULE_LICENSE("GPL");
