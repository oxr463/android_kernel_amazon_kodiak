/*
 * sign_of_life.c
 *
 * Device Sign of Life information
 *
 * Copyright (C) Amazon Technologies Inc. All rights reserved.
 * Yang Liu (yangliu@lab126.com)
 * TODO: Add additional contributor's names.
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 */

#include <linux/init.h>
#include <linux/kernel.h>
#include <linux/module.h>
#include <linux/slab.h>
#include <linux/platform_device.h>
#include <linux/vmalloc.h>
#include <linux/proc_fs.h>
#include <linux/string.h>
#include <linux/sched.h>
#include <linux/uaccess.h>
#include <linux/io.h>
#include <asm/uaccess.h>
#include <linux/sign_of_life.h>

#define DEV_SOL_VERSION     "0.1"
#define DEV_SOL_NAME        "sign_of_life"
#define DEV_BOOT_REASON     "dev_boot_reason"
#define DEV_CRASH_REASON    "dev_crash_reason"
#define MAX_SIZE             10
#define MP_RESET            "mpreset"
#define MPDECISION_NAME     "mpdecision"
#define MPDECISION_RESET    "reset_mp"

static const char * const dev_crash_reason[] = {
	[0] = "no crash",
	[1] = "kernel_panic",
	[2] = "watchdog",
	[3] = "thermal_shutdown",
};

struct dev_sol {
	u8   *data;
        u8   last_crash;
	u32  mp_kill_enable;
	struct mutex lock;
	struct mutex mp_reset_lock;
};
static struct dev_sol *p_dev_sol;

static void kill_mpdecision(void)
{
	int mpdecision_pid = 0;
	struct task_struct *task;

	read_lock(&tasklist_lock);
	for_each_process(task) {
		if ( (strcmp( task->comm, MPDECISION_NAME) == 0 ) ) {
			mpdecision_pid = task->pid;
			printk (KERN_INFO "mpdecision pid is %d\n", mpdecision_pid);
		}
	}
	read_unlock(&tasklist_lock);

	if (mpdecision_pid == 0) {
		printk(KERN_INFO "mpdecision pid is NOT FOUND\n");
		return;
	}

	rcu_read_lock();
	task = pid_task(find_vpid(mpdecision_pid), PIDTYPE_PID);
	if (task)
		send_sig_info(SIGKILL, SEND_SIG_NOINFO, task);
	else
		printk (KERN_INFO "The mpdecision task is NOT FOUND\n");
	rcu_read_unlock();

	return;
}

static int dev_crash_proc_read(char *page, char **start, off_t off, int count,
                                   int *eof, void *data)
{
	int len = 0;
	struct  dev_sol *psol = (struct dev_sol*)data;

	if (!psol)
		return -EINVAL;

	mutex_lock(&psol->lock);
	switch (psol->last_crash)  {
	case DEV_CRASH_KERNEL_PANIC:
		len = strlen(dev_crash_reason[1]) + 1;
		memcpy(page, dev_crash_reason[1], len);
		break;
	case DEV_CRASH_WATCH_DOG:
                len = strlen(dev_crash_reason[2]) + 1;
                memcpy(page, dev_crash_reason[2], len);
		break;
	case DEV_CRASH_THERMAL_SHUTDOWN:
                len = strlen(dev_crash_reason[3]) + 1;
                memcpy(page, dev_crash_reason[3], len);
		break;
	default:
                len = strlen(dev_crash_reason[0]) + 1;
                memcpy(page, dev_crash_reason[0], len);
		break;
	}

	mutex_unlock(&psol->lock);
	printk (KERN_INFO "%s: crash code 0x%x len %d %s\n", DEV_SOL_NAME, psol->last_crash, len, page);
	return len;
}

static int mp_reset_write_proc(struct file *file,const char __user *buf,unsigned long count,void *data )
{
	int  ret;
        char buffer[sizeof(MPDECISION_RESET)];
        size_t len = strnlen(MPDECISION_RESET, sizeof(MPDECISION_RESET));
	struct  dev_sol *psol = (struct dev_sol*)data;

        if (count < sizeof(MPDECISION_RESET)) {
                ret = -EINVAL;
                goto write_proc_failed;
        }

        if(copy_from_user(buffer, buf, len)) {
                ret = -EFAULT;
                goto write_proc_failed;
        }

        if (strncmp(buffer, MPDECISION_RESET, len)) {
                ret = -EINVAL;
		printk (KERN_INFO "%s: recv command is %s\n", __func__, buffer);
		goto write_proc_failed;
        }

	mutex_lock(&psol->mp_reset_lock);
	kill_mpdecision();
	mutex_unlock(&psol->mp_reset_lock);

        return count;

write_proc_failed:
	return ret;
}


static int __init dev_sol_init(void)
{
	struct proc_dir_entry *entry;
	struct proc_dir_entry *mp_kill_entry;
	int status = 0;

	printk(KERN_ERR "Amazon: sign of life device driver init\n");
	p_dev_sol = kzalloc(sizeof(struct dev_sol), GFP_KERNEL);
	if (!p_dev_sol) {
		printk (KERN_INFO "%s: kmalloc allocation failed\n", DEV_SOL_NAME);
		status = -ENOMEM;
		goto init_out1;
	}
	mutex_init(&p_dev_sol->lock);
	mutex_init(&p_dev_sol->mp_reset_lock);

	entry = create_proc_entry(DEV_CRASH_REASON, S_IRUGO | S_IWUSR | S_IWGRP , NULL);
	if (!entry) {
		printk(KERN_ERR "%s: failed to create proc %s entry\n", DEV_SOL_NAME, DEV_CRASH_REASON);
		status = -ENOMEM;
		goto init_out;
	}

	mp_kill_entry = create_proc_entry(MP_RESET, S_IWUGO, NULL);
        if (!entry) {
                printk(KERN_ERR "%s: failed to create proc %s entry\n", DEV_SOL_NAME, MP_RESET);
                status = -ENOMEM;
                goto init_out;
        }

	p_dev_sol->last_crash = get_dev_crash_reason();
	entry->read_proc  = dev_crash_proc_read;
	entry->data = p_dev_sol;

	mp_kill_entry->write_proc = mp_reset_write_proc;
	mp_kill_entry->data = p_dev_sol;
	return 0;

init_out:
	remove_proc_entry(DEV_SOL_NAME, NULL);
init_out1:
	if (p_dev_sol)
		kfree(p_dev_sol);
	return status;
}

static void __exit dev_sol_cleanup(void)
{
	remove_proc_entry(DEV_CRASH_REASON, NULL);
	remove_proc_entry(MP_RESET, NULL);
	if (p_dev_sol)
		kfree(p_dev_sol);
}

late_initcall(dev_sol_init);
module_exit(dev_sol_cleanup);

MODULE_LICENSE("GPL v2");

