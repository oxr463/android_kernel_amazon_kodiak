/*
 * debug_services.c
 *
 * add more debug services including panic trigger, trigger different crashes
 *
 * Copyright (C) Amazon Technologies Inc. All rights reserved.
 * Yang Liu (yangliu@lab126.com)
 * TODO: Add additional contributor's names.
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 */

#include <linux/init.h>
#include <linux/kernel.h>
#include <linux/module.h>
#include <linux/slab.h>
#include <linux/platform_device.h>
#include <linux/vmalloc.h>
#include <linux/proc_fs.h>
#include <linux/string.h>
#include <linux/sched.h>
#include <linux/uaccess.h>
#include <linux/io.h>
#include <linux/delay.h>
#include <linux/uaccess.h>
#include <asm/bugs.h>

#define DS_PROCFS_NAME		"debug_services"
#define DS_PANIC_TRIGGER	"panic_trigger"
#define DS_WDOG_TRIGGER		"wdog_trigger"
#define DS_APPS_WDOG            "apps_wdog"
#define DS_KERNEL_BUG_ON        "bug"
#define DS_KERNEL_PAGE_FAULT    "page_fault"
#define DS_KERNEL_NULL_POINTER  "null_pointer"
#define DS_MAX_PANIC_PATTERN	128
#define DS_MDELAY_TIME		30000

struct debug_services_data {
	struct mutex panic_trigger_lock;
	struct mutex wdog_trigger_lock;
};

static struct debug_services_data *p_data;

struct completion timeout_complete;

static void timeout_work(struct work_struct *work)
{
	pr_info("apps watchdog bark\n");
	preempt_disable();
	mdelay(DS_MDELAY_TIME);
	preempt_enable();
	complete(&timeout_complete);
}

/*
 * trigger the application processor watchdog
 */
static DECLARE_WORK(timeout_work_struct, timeout_work);
static int apps_wdog_bark(void)
{
	init_completion(&timeout_complete);
	schedule_work_on(0, &timeout_work_struct);
	wait_for_completion(&timeout_complete);
	pr_err("Failed to trigger apps bark\n");
	return -EIO;
}

static int panic_trigger_write_proc(struct file *file, const char __user *buf, unsigned long count, void *data)
{
	int  ret;
	char buffer[DS_MAX_PANIC_PATTERN];
	size_t len;
	struct debug_services_data *pds = (struct debug_services_data *)data;

	len = (count < DS_MAX_PANIC_PATTERN) ? count : DS_MAX_PANIC_PATTERN;

	if (copy_from_user(buffer, buf, len)) {
		ret = -EFAULT;
		goto panic_write_proc_failed;
	}

	mutex_lock(&pds->panic_trigger_lock);
	/* check if a BUG needs to be triggered */
	len = strnlen(DS_KERNEL_BUG_ON, sizeof(DS_KERNEL_BUG_ON));
	if (!strncmp(buffer, DS_KERNEL_BUG_ON, len)) {
		printk(KERN_INFO "%s: trigger a kernel bug\n", __func__);
		BUG();
		mutex_unlock(&pds->panic_trigger_lock);
		return ret;
	}

	/* check if a page fault needs to be triggered */
	len = strnlen(DS_KERNEL_PAGE_FAULT, sizeof(DS_KERNEL_PAGE_FAULT));
	if (!strncmp(buffer, DS_KERNEL_PAGE_FAULT, len)) {
		printk(KERN_INFO "%s: trigger a kernel page fault\n", __func__);
		*(int *)(0x40000000) = 10;
		mutex_unlock(&pds->panic_trigger_lock);
		return ret;
	}


	/* check if a NULL POINTER failure needs to be triggered */
	len = strnlen(DS_KERNEL_NULL_POINTER, sizeof(DS_KERNEL_NULL_POINTER));
	if (!strncmp(buffer, DS_KERNEL_NULL_POINTER, len)) {
		printk(KERN_INFO "%s: trigger a NULL POINTER fault\n",
			__func__);
		*(int *)0 = 0;
		mutex_unlock(&pds->panic_trigger_lock);
		return ret;
	}

	/* trigger a panic for the rest of cases */
	panic(buffer);

	mutex_unlock(&pds->panic_trigger_lock);

	return count;

panic_write_proc_failed:
	return ret;
}


static int wdog_trigger_write_proc(struct file *file,
				const char __user *buf,
				unsigned long count, void *data)
{
	int  ret;
	char buffer[sizeof(DS_APPS_WDOG)];
	size_t len = strnlen(DS_APPS_WDOG, sizeof(DS_APPS_WDOG));
	struct debug_services_data *pds = (struct debug_services_data *)data;

	if (count < sizeof(DS_APPS_WDOG)) {
		ret = -EINVAL;
		goto wdog_write_proc_failed;
	}

	if (copy_from_user(buffer, buf, len)) {
		ret = -EFAULT;
		goto wdog_write_proc_failed;
	}

	if (strncmp(buffer, DS_APPS_WDOG, len)) {
		ret = -EINVAL;
		printk (KERN_INFO "%s: recv command %s is not supported\n",
				__func__, buffer);
		printk (KERN_INFO "%s: only %s are supported\n",
				__func__, DS_APPS_WDOG);
		goto wdog_write_proc_failed;
	}

	mutex_lock(&pds->wdog_trigger_lock);
	apps_wdog_bark();
	mutex_unlock(&pds->wdog_trigger_lock);

	return count;

wdog_write_proc_failed:
	return ret;
}

static int __init debug_services_init(void)
{
	struct proc_dir_entry *panic_trigger_entry;
	struct proc_dir_entry *wdog_trigger_entry;
	int status = 0;

	printk(KERN_INFO "%s: initialize the debugging services\n", __func__);
	p_data = kzalloc(sizeof(struct debug_services_data), GFP_KERNEL);
	if (!p_data) {
		printk(KERN_INFO "%s: kmalloc allocation failed\n", __func__);
		return -ENOMEM;
	}

	mutex_init(&p_data->panic_trigger_lock);
	mutex_init(&p_data->wdog_trigger_lock);

	panic_trigger_entry = create_proc_entry(DS_PANIC_TRIGGER, S_IWUGO, NULL);
	if (!panic_trigger_entry) {
		printk(KERN_ERR "%s: failed to create proc %s entry\n",
			__func__, DS_PANIC_TRIGGER);
		status = -ENOMEM;
		goto init_out1;
	}

	wdog_trigger_entry = create_proc_entry(DS_WDOG_TRIGGER, S_IWUGO, NULL);
	if (!wdog_trigger_entry) {
		printk(KERN_ERR "%s: failed to create proc %s entry\n",
			__func__, DS_WDOG_TRIGGER);
		status = -ENOMEM;
		goto init_out;
	}

	panic_trigger_entry->write_proc = panic_trigger_write_proc;
	panic_trigger_entry->data = p_data;

	wdog_trigger_entry->write_proc = wdog_trigger_write_proc;
	wdog_trigger_entry->data = p_data;

	return 0;

init_out:
	remove_proc_entry(DS_PANIC_TRIGGER, NULL);
init_out1:
	if (p_data)
		kfree(p_data);
	return status;
}

static void __exit debug_services_cleanup(void)
{
	remove_proc_entry(DS_PANIC_TRIGGER, NULL);
	if (p_data)
		kfree(p_data);
}

late_initcall(debug_services_init);
module_exit(debug_services_cleanup);

MODULE_LICENSE("GPL v2");

